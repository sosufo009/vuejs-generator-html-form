Vue.component('index', {
    template:
        '<div data-tpl="index" class="wrapper">' +
        '    <navbar @switch-page="switchPage"></navbar>' +
        '    <sidebar @switch-page="switchPage"></sidebar>' +
        '    <div id="pageContent">' +
        '       <div class="main-area" id="mainContent">' +
        '           <component :is="componentIndex" ' +
        '                      transition="fade" ' +
        '                      transition-mode="out-in">' +
        '           </compnent>' +
        '       </div>' +
        '    </div>' +
        '</div>',
    data: function() {
        return {
            //Data
            componentIndex: 'G2G_demo'
        }
    },
    ready: function() {
        //this.pageLoad();
    },
    methods: {
        switchPage: function(child) {
            this.pageLoad(child);
        },
        pageLoad: function(child) {
            var vm = this;
            var id = '#mainContent';
            vm.switchActive = child;
            /*
            $(id).oLoader({
                backgroundColor: 'black',
                fadeInTime: 500,
                fadeOutTime: 1000,
                fadeLevel: 0.5,
            });
            setTimeout(function() {
                $(id).oLoader('hide');
                vm.switchActive = child;
            },
            500);
            */
        }
    },
    computed: {
        switchActive: {
            get: function() {
                return this.componentIndex;
            },
            set: function(val) {
                this.componentIndex = val;
            }
        }
    }
})
